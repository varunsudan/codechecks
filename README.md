# CodeCheck

This project includes dependency for checkstyle & pmd.
The purpose  is to create a template project
which can contain configurations for static code analyzers in one place 
and can be included in other projects thus standardize code quality across different projects.

## Download

```bash
git@gitlab.com:varunsudan/codechecks.git
```

## Run

```
$ cd codechecks
$ gradle tasks --all [to view all CheckStyle/PMD realted tasks]
$ gradle checks [to run all the checks]
$ gradle checkstyleMain[to see violation in src/main]
$ gradle checkstyleTest[to see violation in src/test] 
$ gradle pmdMain[to see violation in src/main]
$ gradle pmdTest[to see violation in src/test] 
```

## Ref
PMD : https://docs.gradle.org/current/userguide/pmd_plugin.html
CheckStyle : https://docs.gradle.org/current/userguide/checkstyle_plugin.html